#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='\e[1;32m \e[0m \w \e[1;34mλ \e[0m'

#export PS1="\[\033[32m\] \[\033[37m\]\[\033[34m\]\w \[\033[0m\]"

colorscript -e panes

